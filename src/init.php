<?php
/**
 * This file registers this plugin with Fubber Framework
 */
if(!isset($GLOBALS['FubberInited']))
    $GLOBALS['FubberInited'] = [];

// This method will be called on each initialization
$GLOBALS['FubberInited'][] = [Fubber\JsApi\JsApi::class, 'register'];
